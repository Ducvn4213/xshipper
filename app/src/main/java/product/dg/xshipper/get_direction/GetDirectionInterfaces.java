package product.dg.xshipper.get_direction;

import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import product.dg.xshipper.model.DirectionLocation;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.model.GigTicketWithDirection;
import product.dg.xshipper.model.SnappedPoint;
import product.dg.xshipper.model.SnappedResponse;

public class GetDirectionInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void presentData(List<SnappedPoint> data);
        void presentEndPoints(List<GigTicketWithDirection> data);
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void loadData(LatLng currentLocation);
        void drawEndPoints();
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void presentData(List<SnappedPoint> data);

        LatLng getLocationFromAddress(String address);
        void presentEndPoints(List<GigTicketWithDirection> data);
    }

    interface ProvidedModelOps {
        void loadData(LatLng currentLocation);
        void drawEndPoints();
    }
}
