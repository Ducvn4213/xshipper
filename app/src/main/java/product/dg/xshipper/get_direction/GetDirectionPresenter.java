package product.dg.xshipper.get_direction;

import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipper.model.DirectionLocation;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.model.GigTicketWithDirection;
import product.dg.xshipper.model.SnappedPoint;

public class GetDirectionPresenter implements GetDirectionInterfaces.ProvidedPresenterOps, GetDirectionInterfaces.RequiredPresenterOps {
    private WeakReference<GetDirectionInterfaces.RequiredViewOps> mView;
    private GetDirectionInterfaces.ProvidedModelOps mModel;

    GetDirectionPresenter(GetDirectionInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(GetDirectionInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void setView(GetDirectionInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void loadData(LatLng currentLocation) {
        mModel.loadData(currentLocation);
    }

    @Override
    public void drawEndPoints() {
        mModel.drawEndPoints();
    }

    @Override
    public void showDialog(final int title, final int message) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title, final String message) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void presentData(final List<SnappedPoint> data) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().presentData(data);
            }
        });
    }

    @Override
    public LatLng getLocationFromAddress(String address) {
        Geocoder coder = new Geocoder(mView.get().getActivity());
        List<Address> addressList;
        try {
            addressList = coder.getFromLocationName(address, 1);
            Address location = addressList.get(0);

            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            return latLng;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public void presentEndPoints(final List<GigTicketWithDirection> data) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().presentEndPoints(data);
            }
        });
    }
}
