package product.dg.xshipper.manage_gig.processing;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.GigTicket;

public class ProcessingInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void updateAdapter(GigTicketAdapter adapter);

        void onNoTicket();
        void onHaveTicket();

        void performEndOption(GigTicket data);
        void performError(GigTicket data);
        void performSuccess(GigTicket data);

        void onSubmitErrorSuccess();
        void onSubmitErrorFail();

        void onSubmitSuccessSuccess();
        void onSubmitSuccessFail();
    }

    interface ProvidedPresenterOps {
        void loadData();
        void goToDetail(int position);

        void getImage();

        void end(GigTicket data);
        void error(GigTicket id);
        void success(GigTicket id);
        void submitError(int id, String note);
        void submitSuccess(int id, String note, Bitmap image);
        GigTicket getDataFromPosition(int pos);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void updateAdapter(GigTicketAdapter adapter);
        void updateNewData(List<GigTicket> data);
        GigTicketAdapter createAdapterWithData(List<GigTicket> data);

        void onNoTicket();
        void onHaveTicket();

        void onSubmitErrorSuccess();
        void onSubmitErrorFail();

        void onSubmitSuccessSuccess();
        void onSubmitSuccessFail();
    }

    interface ProvidedModelOps {
        void downloadData();
        int getIdFromPosition(int position);
        void updateNewData(List<GigTicket> data);

        GigTicket getDataFromPosition(int pos);

        void submitError(int id, String note);
        void submitSuccess(int id, String note, Bitmap image);
    }
}
