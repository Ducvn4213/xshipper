package product.dg.xshipper.manage_gig.processing;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import product.dg.xshipper.R;
import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.manage_gig.completed.done.DoneFragment;
import product.dg.xshipper.manage_gig.pick.pick_confirm.PickConfirmFragment;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.nganluong.NLService;
import product.dg.xshipper.util.FS;

import static android.app.Activity.RESULT_OK;

public class ProcessingFragment extends Fragment implements ProcessingInterfaces.RequiredViewOps {

    private ProcessingInterfaces.ProvidedPresenterOps mPresenter;

    ListView mGigListView;
    TextView mNoGig;
    ImageView mConfirmImage;
    Bitmap mBitmapImage;

    ProgressDialog mProgressDialog;

    private static ProcessingFragment instance;
    public static ProcessingFragment getInstance() {
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_gig_fragment, container, false);

        bindingControls(view);
        setupControlEvents();

        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData();

        instance = ProcessingFragment.this;
    }

    private void bindingControls(View view) {
        mGigListView = (ListView) view.findViewById(R.id.lv_gig_manage);
        mNoGig = (TextView) view.findViewById(R.id.no_ticket);
    }

    private void setupControlEvents() {
        mGigListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getParentActivity());
                LayoutInflater inflater = getParentActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.layout_processing_gig_item, null);
                dialogBuilder.setView(dialogView);

                TextView name = (TextView) dialogView.findViewById(R.id.edt_name);
                TextView status = (TextView) dialogView.findViewById(R.id.edt_status);
                TextView size = (TextView) dialogView.findViewById(R.id.edt_size);
                TextView cod = (TextView) dialogView.findViewById(R.id.edt_cod);
                TextView from = (TextView) dialogView.findViewById(R.id.edt_from);
                TextView to = (TextView) dialogView.findViewById(R.id.edt_to);
                TextView cus_name = (TextView) dialogView.findViewById(R.id.edt_customer_name);
                TextView cus_phone = (TextView) dialogView.findViewById(R.id.edt_customer_phone);
                TextView rev_name = (TextView) dialogView.findViewById(R.id.edt_receiver_name);
                TextView rev_phone = (TextView) dialogView.findViewById(R.id.edt_receiver_phone);

                Button end = (Button) dialogView.findViewById(R.id.btn_end);
                Button cancel = (Button) dialogView.findViewById(R.id.btn_cancel);

                final GigTicket data = mPresenter.getDataFromPosition(i);

                if (data == null) {
                    return;
                }

                name.setText("Đơn hàng: " + data.getTitle());
                size.setText("Kích thước: " + data.getSize());
                from.setText("From: " + data.getFrom());
                to.setText("To: " + data.getTo());
                cus_name.setText("Tên người gửi: " + data.getCustomerName());
                cus_phone.setText("Số điện thoại: " + data.getCustomerPhone());
                rev_name.setText("Tên người nhận: " + data.getReceiverName());
                rev_phone.setText("Số điện thoại: " + data.getReceiverPhone());
                status.setText(getString(R.string.gig_ticket_waiting_pick_message));

                if (data.getCOD() == null || data.getCOD().trim().equalsIgnoreCase("")) {
                    cod.setText(getString(R.string.gig_ticket_nocod));
                }
                else {
                    cod.setText(getString(R.string.gig_ticket_cod));
                }

                final android.app.AlertDialog alertDialog = dialogBuilder.create();

                end.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPresenter.end(data);
                        alertDialog.dismiss();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                if (!alertDialog.isShowing()) {
                    alertDialog.show();
                }
            }
        });
    }

    private void init() {
        ProcessingPresenter presenter = new ProcessingPresenter(this);
        ProcessingModel model = new ProcessingModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) ProcessingFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(getParentActivity(), getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void updateAdapter(GigTicketAdapter adapter) {
        if (mGigListView != null) {
            mGigListView.setAdapter(adapter);
        }
    }

    @Override
    public void onNoTicket() {
        mNoGig.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHaveTicket() {
        mNoGig.setVisibility(View.GONE);
    }

    @Override
    public void performEndOption(final GigTicket data) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getParentActivity());
        LayoutInflater inflater = getParentActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_end_gig_item, null);
        dialogBuilder.setView(dialogView);

        TextView name = (TextView) dialogView.findViewById(R.id.edt_name);

        Button error = (Button) dialogView.findViewById(R.id.btn_end);
        Button success = (Button) dialogView.findViewById(R.id.btn_cancel);

        if (data == null) {
            return;
        }

        name.setText("Kết thúc đơn hàng " + data.getTitle());

        final android.app.AlertDialog alertDialog = dialogBuilder.create();

        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.error(data);
                alertDialog.dismiss();
            }
        });

        success.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.success(data);
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    @Override
    public void performError(final GigTicket data) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getParentActivity());
        LayoutInflater inflater = getParentActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_end_confirm_gig_item, null);
        dialogBuilder.setView(dialogView);

        TextView name = (TextView) dialogView.findViewById(R.id.edt_name);
        final EditText note = (EditText) dialogView.findViewById(R.id.edt_note);

        Button confirm = (Button) dialogView.findViewById(R.id.btn_confirm);

        if (data == null) {
            return;
        }

        name.setText("Kết thúc đơn hàng " + data.getTitle());

        final android.app.AlertDialog alertDialog = dialogBuilder.create();

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.submitError(data.getId(), note.getText().toString());
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    @Override
    public void performSuccess(final GigTicket data) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getParentActivity());
        LayoutInflater inflater = getParentActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_end_success_confirm_gig_item, null);
        dialogBuilder.setView(dialogView);

        TextView name = (TextView) dialogView.findViewById(R.id.edt_name);
        mConfirmImage = (ImageView) dialogView.findViewById(R.id.img_confirm);
        final EditText note = (EditText) dialogView.findViewById(R.id.edt_note);

        Button confirm = (Button) dialogView.findViewById(R.id.btn_confirm);

        if (data == null) {
            return;
        }

        name.setText("Kết thúc đơn hàng " + data.getTitle());

        final android.app.AlertDialog alertDialog = dialogBuilder.create();

        mConfirmImage.setOnClickListener(null);
        mConfirmImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.getImage();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.submitSuccess(data.getId(), note.getText().toString(), mBitmapImage);
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FS.PICK_IMAGE_COMFIRM_REQUEST_CODE && resultCode == RESULT_OK) {
            mBitmapImage = (Bitmap) data.getExtras().get("data");
            mConfirmImage.setImageBitmap(mBitmapImage);
        }
    }

    @Override
    public void onSubmitErrorSuccess() {
        showDialog(R.string.dialog_notice_title, R.string.gig_ticket_confirm_error_success);
        mPresenter.loadData();

        PickConfirmFragment pickConfirmFragment = PickConfirmFragment.getInstance();
        DoneFragment doneFragment = DoneFragment.getInstance();

        if (pickConfirmFragment != null) {
            pickConfirmFragment.onResume();
        }

        if (doneFragment != null) {
            doneFragment.onResume();
        }
    }

    @Override
    public void onSubmitErrorFail() {
        showDialog(R.string.dialog_title_error, R.string.gig_ticket_confirm_error_fail);
        mPresenter.loadData();
    }

    @Override
    public void onSubmitSuccessSuccess() {
        showDialog(R.string.dialog_notice_title, R.string.gig_ticket_confirm_success_success);
        mPresenter.loadData();

        PickConfirmFragment pickConfirmFragment = PickConfirmFragment.getInstance();
        DoneFragment doneFragment = DoneFragment.getInstance();

        if (pickConfirmFragment != null) {
            pickConfirmFragment.onResume();
        }

        if (doneFragment != null) {
            doneFragment.onResume();
        }

        NLService.getInstance().isPaid = false;
    }

    @Override
    public void onSubmitSuccessFail() {
        showDialog(R.string.dialog_title_error, R.string.gig_ticket_confirm_success_fail);
        mPresenter.loadData();
    }
}
