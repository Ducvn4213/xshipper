package product.dg.xshipper.manage_gig;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import product.dg.xshipper.MainActivity;
import product.dg.xshipper.R;
import product.dg.xshipper.manage_gig.completed.CompletedFragment;
import product.dg.xshipper.manage_gig.completed.Error.ErrorFragment;
import product.dg.xshipper.manage_gig.book_confirm.BookConfirmFragment;
import product.dg.xshipper.manage_gig.completed.done.DoneFragment;
import product.dg.xshipper.manage_gig.completed.done_cod.DoneCODFragment;
import product.dg.xshipper.manage_gig.pick.PickFragment;
import product.dg.xshipper.manage_gig.processing.ProcessingFragment;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.util.FS;

public class ManageGigFragment extends Fragment implements ViewPager.OnPageChangeListener {

    SmartTabLayout mTab;
    ViewPager mViewPager;

    FragmentPagerItemAdapter mAdapter;

    private static ManageGigFragment instance;

    public static ManageGigFragment getInstance() {
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_ticket, container, false);

        bindingUI(view);
        setupUIData();
        setupControlEvents();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        FSService service = FSService.getInstance();
        mViewPager.setCurrentItem(service.getSubIndex());
        service.setSubIndex(0);

        instance = this;
    }

    private void bindingUI(View view) {
        mTab = (SmartTabLayout) view.findViewById(R.id.stl_tab);
        mViewPager = (ViewPager) view.findViewById(R.id.vp_container);
    }

    private void setupUIData() {
        FragmentPagerItems.Creator creator = FragmentPagerItems.with(getContext());

        Bundle book_confirm = new Bundle();
        book_confirm.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_BOOK_CONFIRM);
        creator.add(R.string.manage_gigs_book_confirm, BookConfirmFragment.class, book_confirm);

        Bundle pick = new Bundle();
        pick.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_PICK);
        creator.add(R.string.manage_gigs_pick, PickFragment.class, pick);

        Bundle processing = new Bundle();
        processing.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_PROCESSING);
        creator.add(R.string.manage_gigs_processing, ProcessingFragment.class, processing);

        Bundle completed = new Bundle();
        completed.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_COMPLETED);
        creator.add(R.string.manage_gigs_completed, CompletedFragment.class, completed);

        mAdapter = new FragmentPagerItemAdapter(getChildFragmentManager(),
                creator.create());

        mViewPager.setAdapter(mAdapter);
        mTab.setCustomTabView(R.layout.layout_smart_tab, R.id.tv_ticket_tab_item);
        mTab.setViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(this);
    }

    private void setupControlEvents() {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    public void setTitleForTabAt(int pos, int title, int nums) {
        TextView tv = (TextView) mTab.getTabAt(pos).findViewById(R.id.tv_ticket_tab_item);
        tv.setText(getString(title) + " [" + nums + "]");
    }

    @Override
    public void onPageSelected(int position) {
        MainActivity mainActivity = (MainActivity) getActivity();
        if (position == 2) {
            mainActivity.showGetDirection();
        }
        else {
            mainActivity.hideGetDirection();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
