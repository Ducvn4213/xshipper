package product.dg.xshipper.manage_gig.pick.waiting_pick;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import product.dg.xshipper.R;
import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.manage_gig.book_confirm.BookConfirmFragment;
import product.dg.xshipper.manage_gig.pick.pick_confirm.PickConfirmFragment;
import product.dg.xshipper.model.GigTicket;

public class WaitingPickFragment extends Fragment implements WaitingPickInterfaces.RequiredViewOps {

    private WaitingPickInterfaces.ProvidedPresenterOps mPresenter;

    ListView mGigListView;
    TextView mNoGig;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_gig_fragment, container, false);

        bindingControls(view);
        setupControlEvents();

        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData();
    }

    private void bindingControls(View view) {
        mGigListView = (ListView) view.findViewById(R.id.lv_gig_manage);
        mNoGig = (TextView) view.findViewById(R.id.no_ticket);
    }

    private void setupControlEvents() {
        mGigListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getParentActivity());
                LayoutInflater inflater = getParentActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.layout_confirm_gig_item, null);
                dialogBuilder.setView(dialogView);

                TextView name = (TextView) dialogView.findViewById(R.id.edt_name);
                TextView status = (TextView) dialogView.findViewById(R.id.edt_status);
                TextView size = (TextView) dialogView.findViewById(R.id.edt_size);
                TextView cod = (TextView) dialogView.findViewById(R.id.edt_cod);
                TextView from = (TextView) dialogView.findViewById(R.id.edt_from);
                TextView to = (TextView) dialogView.findViewById(R.id.edt_to);
                TextView cus_name = (TextView) dialogView.findViewById(R.id.edt_customer_name);
                TextView cus_phone = (TextView) dialogView.findViewById(R.id.edt_customer_phone);


                Button confirm = (Button) dialogView.findViewById(R.id.btn_confirm);
                Button cancel = (Button) dialogView.findViewById(R.id.btn_cancel);

                final GigTicket data = mPresenter.getDataFromPosition(i);

                if (data == null) {
                    return;
                }

                name.setText("Đơn hàng: " + data.getTitle());
                size.setText("Kích thước: " + data.getSize());
                from.setText("From: " + data.getFrom());
                to.setText("To: " + data.getTo());
                cus_name.setText("Tên người gửi: " + data.getCustomerName());
                cus_phone.setText("Số điện thoại: " + data.getCustomerPhone());
                status.setText(getString(R.string.gig_ticket_waiting_pick_message));

                if (data.getCOD() == null || data.getCOD().trim().equalsIgnoreCase("")) {
                    cod.setText(getString(R.string.gig_ticket_nocod));
                }
                else {
                    cod.setText(getString(R.string.gig_ticket_cod));
                }

                final android.app.AlertDialog alertDialog = dialogBuilder.create();

                confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPresenter.confirm(data.getId());
                        alertDialog.dismiss();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPresenter.cancel(data.getId());
                        alertDialog.dismiss();
                    }
                });

                if (!alertDialog.isShowing()) {
                    alertDialog.show();
                }
            }
        });
    }

    private void init() {
        WaitingPickPresenter presenter = new WaitingPickPresenter(this);
        WaitingPickModel model = new WaitingPickModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) WaitingPickFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void updateAdapter(GigTicketAdapter adapter) {
        if (mGigListView != null) {
            mGigListView.setAdapter(adapter);
        }
    }

    @Override
    public void performReload() {
        mPresenter.loadData();

        BookConfirmFragment bookConfirmFragment = BookConfirmFragment.getIntance();
        PickConfirmFragment pickConfirmFragment = PickConfirmFragment.getInstance();

        if (bookConfirmFragment != null) {
            bookConfirmFragment.onResume();
        }

        if (pickConfirmFragment != null) {
            pickConfirmFragment.onResume();
        }
    }

    @Override
    public void onNoTicket() {
        mNoGig.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHaveTicket() {
        mNoGig.setVisibility(View.GONE);
    }

    @Override
    public void onConfirmOK() {
        showDialog(R.string.dialog_notice_title, R.string.gig_ticket_pick_confirm_ok);
    }

    @Override
    public void onConfirmFail() {
        showDialog(R.string.dialog_title_error, R.string.gig_ticket_pick_confirm_fail);
    }

    @Override
    public void onCancelOK() {
        showDialog(R.string.dialog_notice_title, R.string.gig_ticket_pick_cancel_ok);
    }

    @Override
    public void onCancelFail() {
        showDialog(R.string.dialog_title_error, R.string.gig_ticket_pick_cancel_fail);
    }
}
