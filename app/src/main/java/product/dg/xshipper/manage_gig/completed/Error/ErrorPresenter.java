package product.dg.xshipper.manage_gig.completed.Error;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.GigTicket;

public class ErrorPresenter implements ErrorInterfaces.ProvidedPresenterOps, ErrorInterfaces.RequiredPresenterOps {
    private WeakReference<ErrorInterfaces.RequiredViewOps> mView;
    private ErrorInterfaces.ProvidedModelOps mModel;

    ErrorPresenter(ErrorInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(ErrorInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void loadData() {
        mModel.downloadData();
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void goToDetail(int position) {
    }

    @Override
    public void updateAdapter(GigTicketAdapter adapter) {
        mView.get().updateAdapter(adapter);
    }

    @Override
    public void updateNewData(final List<GigTicket> data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mModel.updateNewData(data);
            }
        });
    }

    @Override
    public GigTicketAdapter createAdapterWithData(List<GigTicket> data) {
        GigTicketAdapter adapter = new GigTicketAdapter(mView.get().getParentActivity(), data);
        return adapter;
    }

    @Override
    public void onNoTicket() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onNoTicket();
            }
        });
    }

    @Override
    public void onHaveTicket() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onHaveTicket();
            }
        });
    }
}
