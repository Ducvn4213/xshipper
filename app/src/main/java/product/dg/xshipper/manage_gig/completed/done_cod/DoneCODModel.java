package product.dg.xshipper.manage_gig.completed.done_cod;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.manage_gig.completed.CompletedFragment;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.response.GigTicketResponse;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;

public class DoneCODModel implements DoneCODInterfaces.ProvidedModelOps {
    private DoneCODInterfaces.RequiredPresenterOps mPresenter;
    private GigTicketAdapter mAdapter;
    private FSService mService = FSService.getInstance();

    DoneCODModel(DoneCODInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;

        List<GigTicket> data = new ArrayList<>();
        mAdapter = mPresenter.createAdapterWithData(data);
        mPresenter.updateAdapter(mAdapter);
    }

    @Override
    public void downloadData() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_status", "4"));

        mService.get_book_gig_ticket(paramList, new FSService.Callback<GigTicketResponse>() {
            @Override
            public void onSuccess(GigTicketResponse data) {
                mPresenter.updateNewData(data.getData());
                mPresenter.onHaveTicket();
            }

            @Override
            public void onFail(String error) {
                List<GigTicket> data = new ArrayList<>();
                mPresenter.updateNewData(data);
                mPresenter.onNoTicket();
            }
        });
    }

    @Override
    public int getIdFromPosition(int position) {
        return mAdapter.getItem(position).getId();
    }

    @Override
    public void updateNewData(List<GigTicket> data) {

        CompletedFragment.getInstance().setTitleForTabAt(1, R.string.manage_gigs_done_cod, data.size());

        mAdapter.clear();
        mAdapter.addAll(data);
        mAdapter.notifyDataSetChanged();
    }
}
