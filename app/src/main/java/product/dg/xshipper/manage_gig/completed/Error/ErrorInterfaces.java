package product.dg.xshipper.manage_gig.completed.Error;

import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.GigTicket;

public class ErrorInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void updateAdapter(GigTicketAdapter adapter);

        void onNoTicket();
        void onHaveTicket();
    }

    interface ProvidedPresenterOps {
        void loadData();
        void goToDetail(int position);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void updateAdapter(GigTicketAdapter adapter);
        void updateNewData(List<GigTicket> data);
        GigTicketAdapter createAdapterWithData(List<GigTicket> data);

        void onNoTicket();
        void onHaveTicket();
    }

    interface ProvidedModelOps {
        void downloadData();
        int getIdFromPosition(int position);
        void updateNewData(List<GigTicket> data);
    }
}
