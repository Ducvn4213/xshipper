package product.dg.xshipper.manage_gig.pick.waiting_pick;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.manage_gig.pick.PickFragment;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.response.GigTicketResponse;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;

public class WaitingPickModel implements WaitingPickInterfaces.ProvidedModelOps {
    private WaitingPickInterfaces.RequiredPresenterOps mPresenter;
    private GigTicketAdapter mAdapter;
    private FSService mService = FSService.getInstance();

    WaitingPickModel(WaitingPickInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;

        List<GigTicket> data = new ArrayList<>();
        mAdapter = mPresenter.createAdapterWithData(data);
        mPresenter.updateAdapter(mAdapter);
    }

    @Override
    public void downloadData() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_status", "2"));

        mService.get_book_gig_ticket(paramList, new FSService.Callback<GigTicketResponse>() {
            @Override
            public void onSuccess(GigTicketResponse data) {
                mPresenter.updateNewData(data.getData());
                mPresenter.onHaveTicket();
            }

            @Override
            public void onFail(String error) {
                List<GigTicket> data = new ArrayList<>();
                mPresenter.updateNewData(data);
                mPresenter.onNoTicket();
            }
        });
    }

    @Override
    public int getIdFromPosition(int position) {
        return mAdapter.getItem(position).getId();
    }

    @Override
    public void updateNewData(List<GigTicket> data) {

        PickFragment.getInstance().setTitleForTabAt(0, R.string.manage_gigs_waiting_pick, data.size());

        mAdapter.clear();
        mAdapter.addAll(data);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public GigTicket getDataFromPosition(int pos) {
        return mAdapter.getItem(pos);
    }

    @Override
    public void confirm(int id) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", "" + id));

        mService.confirmPicked(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.onConfirmOK();
            }

            @Override
            public void onFail(String error) {
                mPresenter.onConfirmFail();
            }
        });
    }

    @Override
    public void cancel(int id) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", "" + id));

        mService.cancelPicked(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.onCancelOK();
            }

            @Override
            public void onFail(String error) {
                mPresenter.onCancelFail();
            }
        });
    }
}
