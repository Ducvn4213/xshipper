package product.dg.xshipper.profile;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.model.User;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;
import product.dg.xshipper.util.Utils;

public class ProfileModel implements ProfileInterfaces.ProvidedModelOps{
    private User mCurrentUser;
    private User mCurrentFacebookUser;

    private Bitmap mCurrentAvatarBitmap;

    private ProfileInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    ProfileModel(ProfileInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setCurrentAvatarBitmap(Bitmap bitmap) {
        this.mCurrentAvatarBitmap = bitmap;
    }

    @Override
    public void doUpdateAvatar() {
        if (mCurrentAvatarBitmap == null) {
            mPresenter.showDialog(R.string.dialog_title_error, R.string.dialog_message_avatar_invalid);
            return;
        }

        Utils.ConvertBitmapToStringTask convertBitmapToStringTask = new Utils.ConvertBitmapToStringTask();
        convertBitmapToStringTask.setBitmap(mCurrentAvatarBitmap);
        convertBitmapToStringTask.setCallback(new Utils.ConvertBitmapToStringCallback() {
            @Override
            public void onSuccess(String bitmapString) {
                doUpdateAvatar(bitmapString);
            }

            @Override
            public void onFail() {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.dialog_message_avatar_invalid);
            }
        });
        convertBitmapToStringTask.execute();
    }

    private void doUpdateAvatar(String bitmapString) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_image", bitmapString));

        mService.update_avatar(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.showDialog(R.string.dialog_notice_title, R.string.dialog_message_avatar_update_success);
                mPresenter.updateAvatarSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.dialog_message_avatar_invalid);
            }
        });
    }

    @Override
    public void doUpdate(String name, String address, String phone) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_name", name));
        paramList.add(new Param("_phone", phone));
        paramList.add(new Param("_address", address));

        mService.update_profile(paramList, new FSService.Callback<User>() {
            @Override
            public void onSuccess(User data) {
                if (mService.getCurrentFacebookUser() != null) {
                    mService.setCurrentFacebookUser(data);
                }
                else {
                    mService.setCurrentUser(data);
                }
                mPresenter.onUpdateSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void setFacebookUser(User user) {
        this.mCurrentFacebookUser = null;
        this.mCurrentUser = null;

        this.mCurrentFacebookUser = user;
    }

    @Override
    public void setUser(User user) {
        this.mCurrentFacebookUser = null;
        this.mCurrentUser = null;

        this.mCurrentUser = user;
    }

    @Override
    public void addMoneyToAccount(final String money) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_money", money));

        mService.add_money_to_wallet(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mService.addMoneyToAccount(money);
                mPresenter.addMoneySuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.addMoneyFail();
            }
        });
    }

    @Override
    public void addMoneyByMobileCardWith(int branch, String number, String seri) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_number", number));
        paramList.add(new Param("_seri", seri));

        if (branch == 3) {
            branch = 6;
        }
        else {
            branch += 1;
        }

        paramList.add(new Param("_branch", branch + ""));

        mService.add_money_by_card_number(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mService.addMoneyToAccount(data);
                mPresenter.addMoneySuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.addMoneyByCardFail();
            }
        });
    }

    @Override
    public User getFacebookUser() {
        return this.mCurrentFacebookUser;
    }

    @Override
    public User getuser() {
        return this.mCurrentUser;
    }

    @Override
    public Boolean isFacebookUser() {
        return this.mCurrentFacebookUser != null;
    }

    @Override
    public void displayUserInfo() {
        if (mService.getCurrentUser() == null) {
            setFacebookUser(mService.getCurrentFacebookUser());
        }
        else {
            setUser(mService.getCurrentUser());
        }

        User currentUser = isFacebookUser() ? mCurrentFacebookUser : mCurrentUser;
        if (currentUser == null) {
            mPresenter.displayUserInfo_None();
        }
        else {
            String avatar = currentUser.getAvatar();
            if (mCurrentAvatarBitmap != null) {
                avatar = null;
            }
            mPresenter.displayUserInfo(currentUser.getName(), currentUser.getEmail(), currentUser.getAddress(), currentUser.getPhone(), avatar, currentUser.getWallet(), currentUser.isVerify(), isFacebookUser());
        }
    }
}