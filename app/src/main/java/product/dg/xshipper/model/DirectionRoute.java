package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DirectionRoute {
    @SerializedName("legs")
    List<DirectionLeg> mDirectionLegs;

    public DirectionRoute () {}

    public List<DirectionLeg> getDirectionLegs() {
        return mDirectionLegs;
    }
}
