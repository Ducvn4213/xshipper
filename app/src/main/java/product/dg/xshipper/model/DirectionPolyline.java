package product.dg.xshipper.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import okhttp3.internal.Util;
import product.dg.xshipper.util.Utils;

public class DirectionPolyline {
    @SerializedName("points")
    String mPoints;

    public DirectionPolyline() {}

    public List<LatLng> getPoints() {
        return Utils.decodePoly(mPoints);
    }
}
