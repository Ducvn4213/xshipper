package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SnappedPoint {
    @SerializedName("location")
    DirectionLocation2 mDirectionLocation2;

    public SnappedPoint() {}

    public DirectionLocation2 getDirectionLocation2() {
        return mDirectionLocation2;
    }
}
