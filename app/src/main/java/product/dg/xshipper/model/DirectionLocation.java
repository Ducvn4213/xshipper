package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

public class DirectionLocation {
    @SerializedName("lat")
    Double mLat;
    @SerializedName("lng")
    Double mLon;

    public DirectionLocation () {}

    public Double getLat() {
        return mLat;
    }

    public Double getLng() {
        return mLon;
    }

    public void setLat(Double data) {
        mLat = data;
    }

    public void setLng(Double data) {
        mLon = data;
    }
}
