package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

public class GigTicket {
    @SerializedName("id")
    int mId;
    @SerializedName("size")
    String mSize;
    @SerializedName("name")
    String mTitle;
    @SerializedName("address")
    String mFrom;
    @SerializedName("address2")
    String mToAddress;
    @SerializedName("ward2")
    String mToWard;
    @SerializedName("district2")
    String mToDistrict;
    @SerializedName("province2")
    String mToProvince;
    @SerializedName("cod")
    String mCOD;

    @SerializedName("cus_name")
    String mCustomerName;
    @SerializedName("cus_phone")
    String mCustomerPhone;

    @SerializedName("rev_name")
    String mReceiverName;
    @SerializedName("rev_phone")
    String mReceiverPhone;

    @SerializedName("distance")
    String mDistance;


    public GigTicket() {}

    public void setId(int data) {
        mId = data;
    }
    public void setSize(String data) {
        mSize = data;
    }
    public void setTitle(String data) {
        mTitle = data;
    }
    public void setFrom(String data) {
        mFrom = data;
    }
    public void setCOD(String cod) {
        mCOD = cod;
    }
    public void setCustomerName (String name) {
        mCustomerName = name;
    }
    public void setCustimerPhone(String phone) {
        mCustomerPhone = phone;
    }


    public int getId() {
        return mId;
    }
    public String getSize() {
        return mSize;
    }
    public String getTitle() {
        return mTitle;
    }
    public String getFrom() {
        return mFrom;
    }
    public String getTo() {
        return mToAddress + ", " + mToWard + ", " + mToDistrict + ", " + mToProvince;
    }
    public String getCOD() {
        return mCOD;
    }
    public String getCustomerName() {
        return mCustomerName;
    }
    public String getCustomerPhone() {
        return mCustomerPhone;
    }
    public String getReceiverName() {
        return mReceiverName;
    }
    public String getReceiverPhone() {
        return mReceiverPhone;
    }
    public String getDistance() {
        if (mDistance != null && !mDistance.trim().equalsIgnoreCase("")) {
            int intDistance = Integer.parseInt(mDistance);
            int du = intDistance % 1000 > 0 ? 1 : 0;
            int reDistance = intDistance/1000 + du;
            return reDistance + " km";
        }
        else {
            return "0 km";
        }
    }
}
