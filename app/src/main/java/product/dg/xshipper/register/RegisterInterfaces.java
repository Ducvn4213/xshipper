package product.dg.xshipper.register;

import android.support.v7.app.AppCompatActivity;

interface RegisterInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void registerSuccessCallback();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void doRegister(String email, String name, String address, String phone, String password);
        void moveToVerify();
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);
        void onRegisterSuccess();
    }

    interface ProvidedModelOps {
        void doRegister(String email, String name, String address, String phone, String password);
    }
}