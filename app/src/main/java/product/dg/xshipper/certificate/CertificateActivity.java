package product.dg.xshipper.certificate;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import product.dg.xshipper.R;
import product.dg.xshipper.certificate.another_certificate.AnotherCertificateActivity;
import product.dg.xshipper.certificate.another_certificate.AnotherCertificateSingleTon;
import product.dg.xshipper.certificate.card.CardUploadSingleTon;
import product.dg.xshipper.util.FS;

public class CertificateActivity extends AppCompatActivity implements CertificateInterfaces.RequiredViewOps {

    ProgressDialog mProgressDialog;
    Button mCMND;
    Button mGPLX;
    Button mHK;
    Button mBLNT;
    Button mOK;

    private CertificateInterfaces.ProvidedPresenterOps mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_certificate);

        bindingControls();
        setupControlEvents();

        init();
    }

    void bindingControls() {
        mCMND = (Button) findViewById(R.id.btn_cmnd);
        mGPLX = (Button) findViewById(R.id.btn_gplx);
        mHK = (Button) findViewById(R.id.btn_hk);
        mBLNT = (Button) findViewById(R.id.btn_blnt);

        mOK = (Button) findViewById(R.id.btn_ok);
    }

    void setupControlEvents() {
        mCMND.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.goToCMND();
            }
        });

        mGPLX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.goToGPLX();
            }
        });

        mHK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.goToHK();
            }
        });

        mBLNT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.goToBLNT();
            }
        });

        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap cmnd_front = CardUploadSingleTon.getInstance().getCMNDFrontSide();
                Bitmap cmnd_back = CardUploadSingleTon.getInstance().getCMNDBackSide();

                Bitmap gplx_front = CardUploadSingleTon.getInstance().getGPLXFrontSide();
                Bitmap gplx_back = CardUploadSingleTon.getInstance().getGPLXBackSide();

                Bitmap cnhk = AnotherCertificateSingleTon.getInstance().getCNHK();
                Bitmap blnt = AnotherCertificateSingleTon.getInstance().getBLNT();

                CardUploadSingleTon cardUploadSingleTon = CardUploadSingleTon.getInstance();
                AnotherCertificateSingleTon anotherCertificateSingleTon = AnotherCertificateSingleTon.getInstance();

                if ((cmnd_front == null && cardUploadSingleTon.mDontCheckCMNDFront == false)
                        || (cmnd_back == null && cardUploadSingleTon.mDontCheckCMNDBack == false)
                        || (gplx_front == null && cardUploadSingleTon.mDontCheckGPLXFront == false)
                        || (gplx_back == null && cardUploadSingleTon.mDontCheckGPLXBack == false)
                        || (cnhk == null && anotherCertificateSingleTon.mDontCheckCNHK == false)
                        || (blnt == null && anotherCertificateSingleTon.mDontCheckBLNT == false)) {
                    showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_certificate);
                    return;
                }

                mPresenter.upload();
            }
        });
    }

    void init() {
        CardUploadSingleTon.getInstance().clearCMND();
        CardUploadSingleTon.getInstance().clearGPLX();
        AnotherCertificateSingleTon.getInstance().clearBLNTList();
        AnotherCertificateSingleTon.getInstance().clearCNHKList();

        CertificatePresenter presenter = new CertificatePresenter(this);
        CertificateModel model = new CertificateModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    @Override
    public AppCompatActivity getActivity() {
        return CertificateActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(CertificateActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(CertificateActivity.this)
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(this, getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onSuccess() {
        final AlertDialog dialog = new AlertDialog.Builder(CertificateActivity.this)
                .setTitle(getString(R.string.dialog_notice_title))
                .setMessage(getString(R.string.dialog_message_certificate_notice))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        CertificateActivity.this.finish();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FS.PICK_CMND_REQUEST_CODE) {
            CardUploadSingleTon cardUploadSingleTon = CardUploadSingleTon.getInstance();
            Bitmap cmndFront = cardUploadSingleTon.getCMNDFrontSide();
            Bitmap cmndBack = cardUploadSingleTon.getCMNDBackSide();

            if (cmndFront != null && cmndBack != null || (cardUploadSingleTon.mDontCheckCMNDFront == true && cardUploadSingleTon.mDontCheckCMNDBack == true))  {
                mCMND.setBackgroundResource(R.drawable.button_rounder_top_blue);
            }
            else {
                mCMND.setBackgroundResource(R.drawable.button_rounder_top_white);
            }
        }

        if (requestCode == FS.PICK_GPLX_REQUEST_CODE) {
            CardUploadSingleTon cardUploadSingleTon = CardUploadSingleTon.getInstance();
            Bitmap gplxFront = cardUploadSingleTon.getGPLXFrontSide();
            Bitmap gplxBack = cardUploadSingleTon.getGPLXBackSide();

            if (gplxFront != null && gplxBack != null || (cardUploadSingleTon.mDontCheckGPLXFront == true && cardUploadSingleTon.mDontCheckGPLXBack == true)) {
                mGPLX.setBackgroundResource(R.drawable.button_rounder_top_blue);
            }
            else {
                mGPLX.setBackgroundResource(R.drawable.button_rounder_top_white);
            }
        }

        if (requestCode == FS.PICK_CNHK_REQUEST_CODE) {
            AnotherCertificateSingleTon anotherCertificateSingleTon = AnotherCertificateSingleTon.getInstance();
            Bitmap cnhk = anotherCertificateSingleTon.getCNHK();

            if (cnhk != null || anotherCertificateSingleTon.mDontCheckCNHK == true) {
                mHK.setBackgroundResource(R.drawable.button_rounder_top_blue);
            }
            else {
                mHK.setBackgroundResource(R.drawable.button_rounder_top_white);
            }
        }

        if (requestCode == FS.PICK_BLNT_REQUEST_CODE) {
            AnotherCertificateSingleTon anotherCertificateSingleTon = AnotherCertificateSingleTon.getInstance();
            Bitmap blnt = anotherCertificateSingleTon.getBLNT();

            if (blnt != null || anotherCertificateSingleTon.mDontCheckBLNT == true) {
                mBLNT.setBackgroundResource(R.drawable.button_rounder_top_blue);
            }
            else {
                mBLNT.setBackgroundResource(R.drawable.button_rounder_top_white);
            }
        }
    }
}
