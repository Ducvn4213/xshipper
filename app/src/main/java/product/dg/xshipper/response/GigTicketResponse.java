package product.dg.xshipper.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import product.dg.xshipper.model.GigTicket;

public class GigTicketResponse {
    @SerializedName("data")
    List<GigTicket> mData;

    public GigTicketResponse() {}

    public List<GigTicket> getData() {
        return mData;
    }
}
