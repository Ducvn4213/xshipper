package product.dg.xshipper.promotion;

import java.lang.ref.WeakReference;

import product.dg.xshipper.R;

/**
 * Created by dg on 3/30/17.
 */

public class PromotionPresenter implements PromotionInterfaces.ProvidedPresenterOps, PromotionInterfaces.RequiredPresenterOps {
    private WeakReference<PromotionInterfaces.RequiredViewOps> mView;
    private PromotionInterfaces.ProvidedModelOps mModel;

    @Override
    public void setView(PromotionInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    PromotionPresenter(PromotionInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(PromotionInterfaces.ProvidedModelOps model) {
        mModel = model;
    }


    @Override
    public void submitCode(String code) {
        if (code.trim().equalsIgnoreCase("")) {
            showDialog(R.string.dialog_title_error, R.string.promotion_code_empty);
            return;
        }

        mView.get().showWaiting();
        mModel.submitCode(code);
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title, final String message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void submitPromotionCodeSuccess() {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().promotionResultCallback();
            }
        });
    }
}
