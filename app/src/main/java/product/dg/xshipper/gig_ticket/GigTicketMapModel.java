package product.dg.xshipper.gig_ticket;

import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.model.User;
import product.dg.xshipper.response.GigTicketResponse;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;

public class GigTicketMapModel implements GigTicketInterfaces.ProvidedModelOps {
    private GigTicketInterfaces.RequiredPresenterOps mPresenter;
    private FSService mService = FSService.getInstance();

    List<GigTicket> mData = new ArrayList<>();

    GigTicketMapModel(GigTicketInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void loadData() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_status", "1"));

        mService.get_gig_ticket(paramList, new FSService.Callback<GigTicketResponse>() {
            @Override
            public void onSuccess(GigTicketResponse data) {
                mData = data.getData();
                mPresenter.presentData(mData);
            }

            @Override
            public void onFail(String error) {
                mData.clear();
                mPresenter.presentData(mData);
            }
        });
    }

    @Override
    public void notifyChange(List<GigTicket> data) {

    }

    @Override
    public GigTicket getDataFromPosition(int pos) {
        return mData.get(pos);
    }

    @Override
    public void bookATicket(int id) {
        User currentUser = mService.getCurrentActiveUser();

        if (!currentUser.isActive() || !currentUser.isVerify()) {
            mPresenter.onDeactiveAccount();
            return;
        }

        if (currentUser.getWallet() <= 0) {
            mPresenter.onEmptyOfWallet();
            return;
        }

        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", id + ""));

        mService.book_gig_ticket(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.bookSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.bookFail();
            }
        });
    }
}
