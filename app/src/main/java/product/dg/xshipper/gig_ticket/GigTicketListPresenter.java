package product.dg.xshipper.gig_ticket;


import android.widget.ArrayAdapter;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.GigTicket;

public class GigTicketListPresenter implements GigTicketInterfaces.ProvidedPresenterOps, GigTicketInterfaces.RequiredPresenterOps {
    private WeakReference<GigTicketInterfaces.RequiredViewOps> mView;
    private GigTicketInterfaces.ProvidedModelOps mModel;

    GigTicketListPresenter(GigTicketInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(GigTicketInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void setView(GigTicketInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void loadData() {
        mModel.loadData();
    }

    @Override
    public void book(int id) {
        mView.get().showWaiting();
        mModel.bookATicket(id);
    }

    @Override
    public GigTicket getDataFromPosition(int id) {
        return mModel.getDataFromPosition(id);
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public GigTicketAdapter createAdapterFromData(List<GigTicket> data) {
        GigTicketAdapter adapter = new GigTicketAdapter(mView.get().getParentActivity(), data);
        return adapter;
    }

    @Override
    public void updateAdapter(GigTicketAdapter adapter) {
        mView.get().updateAdapter(adapter);
    }

    @Override
    public void presentData(List<GigTicket> data) {

    }

    @Override
    public void updateData(final List<GigTicket> data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mModel.notifyChange(data);
            }
        });
    }

    @Override
    public void bookSuccess() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().bookSuccess();
            }
        });
    }

    @Override
    public void bookFail() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().bookFail();
            }
        });
    }

    @Override
    public void onNoTicket() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onNoTicket();
            }
        });
    }

    @Override
    public void onHaveTicket() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onHaveTicket();
            }
        });
    }

    @Override
    public void onEmptyOfWallet() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onEmptyOfWallet();
            }
        });
    }

    @Override
    public void onDeactiveAccount() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onDeactiveAccount();
            }
        });
    }
}
