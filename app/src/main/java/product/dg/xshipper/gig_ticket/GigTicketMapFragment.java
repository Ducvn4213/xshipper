package product.dg.xshipper.gig_ticket;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import product.dg.xshipper.MainActivity;
import product.dg.xshipper.R;
import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.TrackerGPS;
import product.dg.xshipper.util.FS;
import product.dg.xshipper.util.Utils;

public class GigTicketMapFragment extends Fragment implements OnMapReadyCallback, GigTicketInterfaces.RequiredViewOps, LocationListener {

    ProgressDialog mProgressDialog;
    MapView mMapView;
    private GoogleMap mMap;
    HashMap<Marker, GigTicket> mData = new HashMap<>();

    private GigTicketInterfaces.ProvidedPresenterOps mPresenter;

    private AlertDialog mCurDialog;
    private Marker mCurMarker;

    private static GigTicketMapFragment instance;

    public static GigTicketMapFragment getInstance() {
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gig_ticket_map, container, false);

        bindingUI(view, savedInstanceState);
        setupControlEvents();

        init();

        return view;
    }

    public void refreshData() {
        mPresenter.loadData();
    }

    void init() {
        GigTicketMapPresenter presenter = new GigTicketMapPresenter(this);
        GigTicketMapModel model = new GigTicketMapModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    void initMaps() {
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(this);
    }

    void bindingUI(View view, Bundle savedInstanceState) {
        mMapView = (MapView) view.findViewById(R.id.map_view);
        mMapView.onCreate(savedInstanceState);
    }

    void setupControlEvents() {

    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) GigTicketMapFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(getParentActivity(), getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void updateAdapter(GigTicketAdapter adapter) {}

    @Override
    public void presentData(List<GigTicket> data) {
        mMap.clear();
        mData.clear();

        try {
            Geocoder coder = new Geocoder(getParentActivity());
            for (GigTicket d : data) {
                List<Address> address = coder.getFromLocationName(d.getFrom(), 1);
                LatLng latLng = new LatLng(address.get(0).getLatitude(), address.get(0).getLongitude());
                MarkerOptions options = new MarkerOptions();

                BitmapDescriptor icon;
                if (d.getSize().equalsIgnoreCase("nhỏ")) {
                    icon = BitmapDescriptorFactory.fromResource(R.mipmap.gig_small);
                }
                else {
                    icon = BitmapDescriptorFactory.fromResource(R.mipmap.gig_medium);
                }

                options.flat(true);
                options.icon(icon);
                options.position(latLng);
                options.title(d.getTitle());

                Marker marker = mMap.addMarker(options);
                mData.put(marker, d);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void bookSuccess() {
        mCurMarker.remove();
        mCurDialog.dismiss();
        hideWaiting();
        MainActivity mainActivity = (MainActivity) getParentActivity();
        mainActivity.goToTab(1);

        showDialog(R.string.dialog_notice_title, R.string.gig_ticket_book_success);
    }

    @Override
    public void bookFail() {
        mCurDialog.dismiss();
        hideWaiting();
        showDialog(R.string.dialog_title_error, R.string.gig_ticket_book_fail);
    }

    @Override
    public void onNoTicket() {
        showDialog(R.string.dialog_notice_title, R.string.gig_ticket_no_ticket);
    }

    @Override
    public void onHaveTicket() {
        //do nothing
    }

    LocationManager locationManager;
    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;

    public void onMapReady() {
        onMapReady(mMap);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(getParentActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getParentActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);

            FSService mService = FSService.getInstance();
            if (mService.lastLatitude != 0 && mService.lastLongitude !=0) {
                hideWaiting();
                double _longitude = mService.lastLongitude;
                double _latitude = mService.lastLatitude;

                LatLng defaultLocation = new LatLng(_latitude, _longitude);
                mMap.addMarker(new MarkerOptions().position(defaultLocation).title(getString(R.string.get_current_location_your_location)));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(defaultLocation));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
            }
            else {
                locationManager = (LocationManager) getParentActivity().getSystemService(Context.LOCATION_SERVICE);
                Boolean checkGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                Boolean checkNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                if (checkNetwork) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
                }
                else if (checkGPS) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
                }

                showWaiting();
            }
        }


        mMap.setOnMarkerClickListener(null);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onMarkerClick(Marker marker) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getParentActivity());
                LayoutInflater inflater = getParentActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.layout_view_gig_item, null);
                dialogBuilder.setView(dialogView);

                TextView name = (TextView) dialogView.findViewById(R.id.edt_name);
                TextView cod = (TextView) dialogView.findViewById(R.id.edt_cod);
                TextView size = (TextView) dialogView.findViewById(R.id.edt_size);
                TextView from = (TextView) dialogView.findViewById(R.id.edt_from);
                TextView to = (TextView) dialogView.findViewById(R.id.edt_to);
                TextView est = (TextView) dialogView.findViewById(R.id.edt_estimate);

                Button book = (Button) dialogView.findViewById(R.id.btn_book);
                Button viewDetail = (Button) dialogView.findViewById(R.id.btn_viewdetail);

                final GigTicket data = mData.get(marker);
                mCurMarker = marker;

                if (data == null) {
                    return false;
                }

                name.setText(data.getTitle());
                size.setText("Size: " + data.getSize());
                from.setText("From: " + data.getFrom());
                to.setText("To: " + data.getTo());
                est.setText("Distance: " + data.getDistance());

                if (data.getCOD() == null || data.getCOD().trim().equalsIgnoreCase("")) {
                    cod.setText(getString(R.string.gig_ticket_nocod));
                }
                else {
                    cod.setText(getString(R.string.gig_ticket_cod));
                }

                final AlertDialog alertDialog = dialogBuilder.create();

                book.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPresenter.book(data.getId());
                    }
                });

                viewDetail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                if (!alertDialog.isShowing()) {
                    mCurDialog = alertDialog;
                    alertDialog.show();
                }
                return false;
            }
        });

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                mPresenter.loadData();
            }
        }, 1000);
    }

    private LatLng getCurrentLocationLatLng() {
        LatLng defaultLocation = new LatLng(0, 0);
        TrackerGPS trackerGPS = new TrackerGPS(getParentActivity());
        if (trackerGPS.canGetLocation()) {
            if (trackerGPS.getLatitude() == 0 && trackerGPS.getLongitude() == 0) {
                return getCurrentLocationLatLng();
            }
            defaultLocation = new LatLng(trackerGPS.getLatitude(), trackerGPS.getLongitude());
            return defaultLocation;
        }

        return defaultLocation;
    }

    @Override
    public void onResume() {
        super.onResume();
        initMaps();
        mMapView.onResume();

        instance = GigTicketMapFragment.this;
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onEmptyOfWallet() {
        mCurDialog.dismiss();
        hideWaiting();
        showDialog(R.string.dialog_title_error, R.string.dialog_message_empty_money);
    }

    @Override
    public void onDeactiveAccount() {
        mCurDialog.dismiss();
        hideWaiting();
        showDialog(R.string.dialog_title_error, R.string.dialog_message_deactive_account);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras){}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onLocationChanged(Location location) {
        hideWaiting();
        double _longitude = location.getLongitude();
        double _latitude = location.getLatitude();

        LatLng defaultLocation = new LatLng(_latitude, _longitude);
        mMap.addMarker(new MarkerOptions().position(defaultLocation).title(getString(R.string.get_current_location_your_location)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(defaultLocation));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
    }
}
