package product.dg.xshipper.gig_ticket;

import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;

import java.util.List;

import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.GigTicket;

interface GigTicketInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void updateAdapter(GigTicketAdapter adapter);
        void presentData(List<GigTicket> data);

        void bookSuccess();
        void bookFail();

        void onNoTicket();
        void onHaveTicket();

        void onEmptyOfWallet();
        void onDeactiveAccount();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void loadData();

        void book(int id);
        GigTicket getDataFromPosition(int id);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        GigTicketAdapter createAdapterFromData(List<GigTicket> data);
        void updateAdapter(GigTicketAdapter adapter);
        void presentData(List<GigTicket> data);
        void updateData(List<GigTicket> data);

        void bookSuccess();
        void bookFail();

        void onNoTicket();
        void onHaveTicket();

        void onEmptyOfWallet();

        void onDeactiveAccount();
    }

    interface ProvidedModelOps {
        void loadData();
        void notifyChange(List<GigTicket> data);
        GigTicket getDataFromPosition(int id);
        void bookATicket(int id);
    }
}